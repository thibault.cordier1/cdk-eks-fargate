#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import {CdkElkStack} from '../lib/cdk-elk-stack';

import * as dotenv from "dotenv";

dotenv.config();

const account = process.env.account;
const region = process.env.region;

const app_env = {
    region: region,
    account: account
}

const app = new cdk.App();
new CdkElkStack(app, 'B12', {env: app_env});

