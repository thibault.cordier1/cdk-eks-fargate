import { Stack, Construct, StackProps } from '@aws-cdk/core';
import VpcStack from './vpc-stack';
import EksStack from './eks-stack';
import AlbStack from './alb-elk-stack';


export class CdkElkStack extends Stack {


    constructor(scope: Construct, id: string, props?: StackProps) {
        super(scope, id, props);

        // The code that defines your stack goes here

        const vpcStack = new VpcStack(this, 'MyVPC')
        const eksClusterStack = new EksStack(this, 'MyEKSCluster', {
            vpc: vpcStack.myVPC,

        })

    }
}
