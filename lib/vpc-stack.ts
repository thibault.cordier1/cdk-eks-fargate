
import { Vpc, PrivateSubnet, SubnetType } from '@aws-cdk/aws-ec2';
import { CfnOutput, NestedStack } from '@aws-cdk/core';
import cfn = require('@aws-cdk/aws-cloudformation');
import { Stack, Construct, StackProps } from '@aws-cdk/core';

export default class VpcStack extends cfn.NestedStack {

    readonly myVPC: Vpc;

    constructor(scope: Construct, id: string, props?: cfn.NestedStackProps) {
        super(scope, id, props);

        // The code that defines your stack goes here

        this.myVPC = new Vpc(this, "myVPC", {
            cidr: "10.21.0.0/16",
            maxAzs: 2,
            natGateways: 1,
            subnetConfiguration: [
                {
                    name: "ECS-Public-Subnet",
                    subnetType: SubnetType.PUBLIC
                },
                {
                    name: "ECS-Nodes-Subnet",
                    subnetType: SubnetType.PRIVATE
                }
            ]

        })



    }
}
