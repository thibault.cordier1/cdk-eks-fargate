import * as cdk from '@aws-cdk/core';
import * as iam from '@aws-cdk/aws-iam';
import * as eks from '@aws-cdk/aws-eks';
import { SecurityGroup, IVpc, CfnSecurityGroupIngress, Vpc } from '@aws-cdk/aws-ec2';
import { CfnOutput, NestedStack, Stack, CustomResource, Fn } from '@aws-cdk/core';
import * as cfn from '@aws-cdk/aws-cloudformation';
import { ManagedPolicy, PolicyStatement, Effect } from '@aws-cdk/aws-iam';
import { CdkElkStack } from './cdk-elk-stack';
import { getAppMeshCrdsMesh, getAppMeshVirtualNode, getAppMeshVirtualRouter, getAppMeshVirtualService } from './app-mesh-crd';


interface EksStackProps extends cfn.NestedStackProps {
    vpc: IVpc
}

export default class EksStack extends cfn.NestedStack {

    readonly myFargateCluster: eks.FargateCluster;

    constructor(scope: cdk.Construct, id: string, props: EksStackProps) {
        super(scope,id, props);

        // The code that defines your stack goes here
        const nodesSecurityGroup = new SecurityGroup(this, 'EKS-Node-SG', {
            vpc: props.vpc,
            description: "Security Group to allow nodes to communicate",
            allowAllOutbound: true
        })

        const nodeSecurityInboundRule = new CfnSecurityGroupIngress(this, 'NodeInbound', {
            ipProtocol: "-1",
            groupId: nodesSecurityGroup.securityGroupId,
            sourceSecurityGroupId: nodesSecurityGroup.securityGroupId
        })


        const clusterRole = new iam.Role(this, 'EKSClusterRole', {
            assumedBy: new iam.ServicePrincipal("eks.amazonaws.com"),
            managedPolicies: [
                ManagedPolicy.fromAwsManagedPolicyName("AmazonEKSClusterPolicy")
            ]
        })

        const podExecutionRole = new iam.Role(this, 'EKSFargatePodExecutionRole', {
            assumedBy: new iam.ServicePrincipal("eks-fargate-pods.amazonaws.com"),
            managedPolicies: [
                ManagedPolicy.fromAwsManagedPolicyName("AmazonEKSFargatePodExecutionRolePolicy")
            ]
        })

        const AdministratorsIamPolicy = new iam.Policy(this, 'EKS-ADMINISTRATOR-IAM-Policy', {
            statements: [
                new PolicyStatement({
                    effect: Effect.ALLOW,
                    resources: ["*"],
                    actions: [
                        "eks:DescribeCluster"
                    ]
                })
            ]
        })

        const AdministratorRole = new iam.Role(this, 'AdministratorsOfEKSCluster', {
            assumedBy: new iam.AccountPrincipal(Stack.of(this).account),
        })
        AdministratorRole.attachInlinePolicy(AdministratorsIamPolicy)

        this.myFargateCluster = new eks.FargateCluster(this, 'FC', {
            version: eks.KubernetesVersion.V1_17,
            vpc: props.vpc,
            mastersRole: AdministratorRole,
            role: clusterRole,
            securityGroup: nodesSecurityGroup
        })
        

        const ALBIamPolicy = new iam.Policy(this, 'EKS-ALB-IAM-Policy', {
            statements: [
                new PolicyStatement({
                    effect: Effect.ALLOW,
                    resources: ["*"],
                    actions: [
                        "acm:DescribeCertificate",
                        "acm:ListCertificates",
                        "acm:GetCertificate"
                    ]
                }),
                new PolicyStatement({
                    effect: Effect.ALLOW,
                    resources: ["*"],
                    actions: [
                        "ec2:AuthorizeSecurityGroupIngress",
                        "ec2:CreateSecurityGroup",
                        "ec2:CreateTags",
                        "ec2:DeleteTags",
                        "ec2:DeleteSecurityGroup",
                        "ec2:DescribeAccountAttributes",
                        "ec2:DescribeAddresses",
                        "ec2:DescribeInstances",
                        "ec2:DescribeInstanceStatus",
                        "ec2:DescribeInternetGateways",
                        "ec2:DescribeNetworkInterfaces",
                        "ec2:DescribeSecurityGroups",
                        "ec2:DescribeSubnets",
                        "ec2:DescribeTags",
                        "ec2:DescribeVpcs",
                        "ec2:ModifyInstanceAttribute",
                        "ec2:ModifyNetworkInterfaceAttribute",
                        "ec2:RevokeSecurityGroupIngress"
                    ]
                }),
                new PolicyStatement({
                    effect: Effect.ALLOW,
                    resources: ["*"],
                    actions: [
                        "elasticloadbalancing:AddListenerCertificates",
                        "elasticloadbalancing:AddTags",
                        "elasticloadbalancing:CreateListener",
                        "elasticloadbalancing:CreateLoadBalancer",
                        "elasticloadbalancing:CreateRule",
                        "elasticloadbalancing:CreateTargetGroup",
                        "elasticloadbalancing:DeleteListener",
                        "elasticloadbalancing:DeleteLoadBalancer",
                        "elasticloadbalancing:DeleteRule",
                        "elasticloadbalancing:DeleteTargetGroup",
                        "elasticloadbalancing:DeregisterTargets",
                        "elasticloadbalancing:DescribeListenerCertificates",
                        "elasticloadbalancing:DescribeListeners",
                        "elasticloadbalancing:DescribeLoadBalancers",
                        "elasticloadbalancing:DescribeLoadBalancerAttributes",
                        "elasticloadbalancing:DescribeRules",
                        "elasticloadbalancing:DescribeSSLPolicies",
                        "elasticloadbalancing:DescribeTags",
                        "elasticloadbalancing:DescribeTargetGroups",
                        "elasticloadbalancing:DescribeTargetGroupAttributes",
                        "elasticloadbalancing:DescribeTargetHealth",
                        "elasticloadbalancing:ModifyListener",
                        "elasticloadbalancing:ModifyLoadBalancerAttributes",
                        "elasticloadbalancing:ModifyRule",
                        "elasticloadbalancing:ModifyTargetGroup",
                        "elasticloadbalancing:ModifyTargetGroupAttributes",
                        "elasticloadbalancing:RegisterTargets",
                        "elasticloadbalancing:RemoveListenerCertificates",
                        "elasticloadbalancing:RemoveTags",
                        "elasticloadbalancing:SetIpAddressType",
                        "elasticloadbalancing:SetSecurityGroups",
                        "elasticloadbalancing:SetSubnets",
                        "elasticloadbalancing:SetWebACL"
                    ]
                }),
                new PolicyStatement({
                    effect: Effect.ALLOW,
                    resources: ["*"],
                    actions: [
                        "iam:CreateServiceLinkedRole",
                        "iam:GetServerCertificate",
                        "iam:ListServerCertificates"
                    ]
                }),
                new PolicyStatement({
                    effect: Effect.ALLOW,
                    resources: ["*"],
                    actions: [
                        "cognito-idp:DescribeUserPoolClient"
                    ]
                }),
                new PolicyStatement({
                    effect: Effect.ALLOW,
                    resources: ["*"],
                    actions: [
                        "waf-regional:GetWebACLForResource",
                        "waf-regional:GetWebACL",
                        "waf-regional:AssociateWebACL",
                        "waf-regional:DisassociateWebACL"
                    ]
                }),
                new PolicyStatement({
                    effect: Effect.ALLOW,
                    resources: ["*"],
                    actions: [
                        "tag:GetResources",
                        "tag:TagResources"
                    ]
                }),
                new PolicyStatement({
                    effect: Effect.ALLOW,
                    resources: ["*"],
                    actions: [
                        "waf:GetWebACL"
                    ]
                }),

            ],
            policyName: 'ALBIngressControllerIAMPolicy'
        })

        const sa = this.myFargateCluster.addServiceAccount('alb-ingress-controller', {
            namespace: "kube-system",
        })

        const ActivateALBRBACResource = new eks.KubernetesResource(this, 'ALBRBACResource', {
            cluster: this.myFargateCluster,
            manifest: [
                {
                    "apiVersion": "rbac.authorization.k8s.io/v1",
                    "kind": "ClusterRole",
                    "metadata": {
                        "labels": {
                            "app.kubernetes.io/name": "alb-ingress-controller"
                        },
                        "name": "alb-ingress-controller"
                    },
                    "rules": [
                        {
                            "apiGroups": [
                                "",
                                "extensions"
                            ],
                            "resources": [
                                "configmaps",
                                "endpoints",
                                "events",
                                "ingresses",
                                "ingresses/status",
                                "services",
                                "pods/status"
                            ],
                            "verbs": [
                                "create",
                                "get",
                                "list",
                                "update",
                                "watch",
                                "patch"
                            ]
                        },
                        {
                            "apiGroups": [
                                "",
                                "extensions"
                            ],
                            "resources": [
                                "nodes",
                                "pods",
                                "secrets",
                                "services",
                                "namespaces"
                            ],
                            "verbs": [
                                "get",
                                "list",
                                "watch"
                            ]
                        }
                    ]
                },
                {
                    "apiVersion": "rbac.authorization.k8s.io/v1",
                    "kind": "ClusterRoleBinding",
                    "metadata": {
                        "labels": {
                            "app.kubernetes.io/name": "alb-ingress-controller"
                        },
                        "name": "alb-ingress-controller"
                    },
                    "roleRef": {
                        "apiGroup": "rbac.authorization.k8s.io",
                        "kind": "ClusterRole",
                        "name": "alb-ingress-controller"
                    },
                    "subjects": [
                        {
                            "kind": "ServiceAccount",
                            "name": sa.serviceAccountName,
                            "namespace": "kube-system"
                        }
                    ]
                }

            ]
        })

        ActivateALBRBACResource.node.addDependency(sa)
        sa.role.attachInlinePolicy(ALBIamPolicy)
        new cdk.CfnOutput(this, 'ServiceAccountIamRole', { value: sa.role.roleArn })


        const albDeployment = this.myFargateCluster.addResource('ALB-Deployment', {
            apiVersion: "apps/v1",
            kind: "Deployment",
            metadata: {
                labels: {
                    "app.kubernetes.io/name": "alb-ingress-controller"
                },
                name: "alb-ingress-controller",
                namespace: "kube-system"
            },
            spec: {
                selector: {
                    matchLabels: {
                        "app.kubernetes.io/name": "alb-ingress-controller"
                    }
                },
                template: {
                    metadata: {
                        labels: {
                            "app.kubernetes.io/name": "alb-ingress-controller"
                        }
                    },
                    spec: {
                        containers: [
                            {
                                name: "alb-ingress-controller",
                                args: [
                                    "--ingress-class=alb",
                                    Fn.join("", ["--cluster-name=", this.myFargateCluster.clusterName]),
                                    Fn.join("", ["--aws-vpc-id=", props.vpc.vpcId]),
                                    Fn.join("", ["--aws-region=", Stack.of(this).region])


                                ],
                                image: "docker.io/amazon/aws-alb-ingress-controller:v1.1.4"
                            }
                        ],
                        serviceAccountName: sa.serviceAccountName
                    }
                }
            }
        });

        albDeployment.node.addDependency(sa)
        
        this.generateAppMeshResources()

     


    }

    generateAppMeshResources() {

        const serviceAccountIAMRoleMesh = new iam.Policy(this, 'AppMeshSAPolicy', {
            statements: [
                new PolicyStatement({
                    effect: Effect.ALLOW,
                    resources: ["*"],
                    actions: [
                        "appmesh:*",
                        "servicediscovery:CreateService",
                        "servicediscovery:DeleteService",
                        "servicediscovery:GetService",
                        "servicediscovery:GetInstance",
                        "servicediscovery:RegisterInstance",
                        "servicediscovery:DeregisterInstance",
                        "servicediscovery:ListInstances",
                        "servicediscovery:ListNamespaces",
                        "servicediscovery:ListServices",
                        "servicediscovery:GetOperation",
                        "servicediscovery:GetInstancesHealthStatus",
                        "servicediscovery:UpdateInstanceCustomHealthStatus",
                        "route53:GetHealthCheck",
                        "route53:CreateHealthCheck",
                        "route53:UpdateHealthCheck",
                        "route53:ChangeResourceRecordSets",
                        "route53:DeleteHealthCheck"
                    ]
                }),
            ]
        })



        const appMeshFargateProfile = new eks.FargateProfile(this, 'APP-MESH-FARGATE-PROFILE', {
            cluster: this.myFargateCluster,
            selectors: [
                { namespace: 'appmesh-system' },
                { namespace: 'appmesh-application' }
            ]
        })
        
        const AppMeshSA = this.myFargateCluster.addServiceAccount('appmesh-sa', {
            namespace: "appmesh-system",
        })
        

        const AppMeshSystemNS = new eks.KubernetesResource(this, 'APP-MESH-SYSTEM-NS', {
            cluster: this.myFargateCluster,
            manifest: [{
                    "apiVersion": "v1",
                    "kind": "Namespace",
                    "metadata": {
                        "name": "appmesh-system",
                    }
                }]
        })

        const AppMeshApplicationNS = new eks.KubernetesResource(this, 'APP-MESH-APPLICATION-NS', {
            cluster: this.myFargateCluster,
            manifest: [{
                    "apiVersion": "v1",
                    "kind": "Namespace",
                    "metadata": {
                        "name": "appmesh-application",
                        "labels": {
                            "appmesh.k8s.aws/sidecarInjectorWebhook": "enabled"
                        }
                    }
                }]
        })

        // Wait for Fargate Profile Creation to handle "appmesh-system","appmesh-application"
        AppMeshSystemNS.node.addDependency(appMeshFargateProfile)
        AppMeshApplicationNS.node.addDependency(appMeshFargateProfile)
        // Wait for namespace creation: appmesh-system
        AppMeshSA.node.addDependency(AppMeshSystemNS)
        // Attach matching IAM Role to Service Account
        AppMeshSA.role.attachInlinePolicy(serviceAccountIAMRoleMesh)


        new CfnOutput(this, 'AppMeshServiceAccountName', {
            value: AppMeshSA.serviceAccountName
        })







        const AppMeshApplicationServiceAccount = new eks.ServiceAccount(this, 'APPMESH-APP-SA-DEFAULT', {
            cluster: this.myFargateCluster,
            namespace: "appmesh-application",
            name: "default"
        })


        const APPMEeshSArole = AppMeshApplicationServiceAccount.role
        //Attach AWS Managed Policies
        APPMEeshSArole.addManagedPolicy(ManagedPolicy.fromAwsManagedPolicyName("AWSAppMeshEnvoyAccess"))
        APPMEeshSArole.addManagedPolicy(ManagedPolicy.fromAwsManagedPolicyName('AWSCloudMapDiscoverInstanceAccess'))
        APPMEeshSArole.addManagedPolicy(ManagedPolicy.fromAwsManagedPolicyName('AWSXRayDaemonWriteAccess'))
        APPMEeshSArole.addManagedPolicy(ManagedPolicy.fromAwsManagedPolicyName('CloudWatchLogsFullAccess'))
        APPMEeshSArole.addManagedPolicy(ManagedPolicy.fromAwsManagedPolicyName('AWSAppMeshFullAccess'))
        APPMEeshSArole.addManagedPolicy(ManagedPolicy.fromAwsManagedPolicyName('AWSCloudMapFullAccess'))
        
        AppMeshApplicationServiceAccount.node.addDependency(AppMeshApplicationNS)


        const AMCHelmChart = new eks.HelmChart(this,'AMCHelmChart', {
            chart: "appmesh-controller",
            release: "appmesh-controller",
            repository: "https://aws.github.io/eks-charts",
            cluster: this.myFargateCluster,
            version: "1.0.0",
            values: {
                "serviceAccount": {
                    "create": false,
                    "name": AppMeshSA.serviceAccountName
                },
                "region": Stack.of(this).region
            },
            namespace: "appmesh-system"
        })

        AMCHelmChart.node.addDependency(AppMeshApplicationServiceAccount)

        const appMeshCustomResources = new eks.KubernetesResource(this, 'AppMeshCR', {
            cluster: this.myFargateCluster,
            manifest: [
                getAppMeshCrdsMesh(),
                getAppMeshVirtualNode(),
                getAppMeshVirtualRouter(),
                getAppMeshVirtualService()
            ]
        })

        appMeshCustomResources.node.addDependency(AppMeshSystemNS)
        
    }

}
